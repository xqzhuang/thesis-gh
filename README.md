# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Maintenance tool for greenhouse WSN network deployments
* 1.0

### Summary of set up ###

* Django==1.8.2
* argparse==1.2.1
* django-activelink==0.4
* django-endless-pagination==2.0
* django-extensions==1.5.5
* django-geojson==2.8.0
* jsonfield==1.0.3
* six==1.9.0
* utm==0.4.0
* wsgiref==0.1.2
* Configuration

### Dependencies ###
* Geospatial libraries - $sudo apt-get install binutils libproj-dev gdal-bin
* $pip install django-activelink
* $pip install django_extensions
* $pip install django-geojson
* $pip install jsonfield
* $pip install django-endless-pagination
* $pip install utm



* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
