from django.db import models
from django.utils.encoding import smart_unicode
# Create your models here.

class HistoryRecord(models.Model):
	nnumber = models.CharField(max_length=20)
	operation = models.CharField(max_length=20, default='unknown')
	battery_number = models.CharField(max_length=20)
	rumber = models.CharField(max_length=20)
	location_top = models.FloatField()
	location_left = models.FloatField()
	location_height = models.FloatField()
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False) #the time when the user sign up

	def __unicode__(self):
		return "History: " + smart_unicode(self.nnumber)