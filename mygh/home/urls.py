from django.conf.urls import include, url

urlpatterns = [
	url(r'^history/$', 'home.views.history', name='history'),
	url(r'^about/$', 'home.views.about', name='about'),
]
