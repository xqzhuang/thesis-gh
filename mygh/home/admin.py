from django.contrib import admin

from .models import *

class HistoryRecordAdmin(admin.ModelAdmin):
	list_display = ['nnumber', 'operation', 'battery_number', 'rumber', 
	'location_top', 'location_left', 'location_height', 'timestamp']
	class Meta:
		model = HistoryRecord

admin.site.register(HistoryRecord, HistoryRecordAdmin)