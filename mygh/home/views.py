from django.shortcuts import render, render_to_response, RequestContext
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.db.models import Max, Count

from .models import *
from nodes.models import *
from rooms.models import Background

from djgeojson.serializers import Serializer as GeoJSONSerializer
from djgeojson.views import GeoJSONLayerView


def main(request):
	# I pass the background here for navbar background switching, distinct name
	backgrounds = Background.objects.values('name').annotate(scount=Count('name'))

	return render_to_response("main.html", 
							locals(),
							context_instance=RequestContext(request))

def history(request):
	records = HistoryRecord.objects.all().order_by('-timestamp')

	record_data = {
		"record_detail" : records
	}

	return render_to_response("history.html", 
							record_data,
							context_instance=RequestContext(request))


def about(request):
	return render_to_response("about.html", 
							locals(),
							context_instance=RequestContext(request))