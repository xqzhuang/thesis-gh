///////////////////////////////////////////////////////////////////////////////////////////////////
//Switch background, simple achieve it with changing visibility of layer
//TODO: 1. different image may need to set different image size and extend
//      Now I just set to the biggest one which includes all rooms
//      2. image url cannot hardcode.
///////////////////////////////////////////////////////////////////////////////////////////////////
var pixelProjection = new ol.proj.Projection({
  code: 'pixel',
  units: 'pixels',
  extent: [0, 0, 958, 1688] //[minX, minY, maxX, maxY]
});

var createImageLayer = function(imageName){
	var imageUrl = 'static/img/' + imageName + '.jpg';

	// Create an image layer
	var imageLayer = new ol.layer.Image({
		name: imageName,
		opacity: 0.75,
		source: new ol.source.ImageStatic({
			attributions: [
				new ol.Attribution({
					html: '&copy; Wroclaw Univercity of Technology'
				})
			],
			url: imageUrl,
			imageSize: [958, 1688],
			projection: pixelProjection,
			// extend [minX, minY, maxX, maxY]
			imageExtent: pixelProjection.getExtent()
		})
	});
	return imageLayer;
}

//Set basic map layers to invisible
var setInvisible = function(){
	layerOSM.setVisible(false);
	nodeVector.setVisible(false);
	roomVector.setVisible(false);
}

//Set basic map layers to visible, we use OSM by default
var setVisible = function(){
	
	var layers = map.getLayerGroup().getLayers().getArray();
	var len = layers.length;
	for(var i = 0; i < len; i++){
		if (layers[i].get('name') === "OSM" || layers[i].get('name') === "room" || layers[i].get('name') === "node") {
			layers[i].setVisible(true);
		} else{
			layers[i].setVisible(false);
		}
	}
}

/**
 * Set selected image layer to be visible, other layers simply set to be invisible 
 * @param {ol.layer.Base} layer
 * @param {String} layerName
 * @returns 
 */
var setImageLayerVisible = function(layerName){
	var layers = map.getLayerGroup().getLayers().getArray();
	var len = layers.length;
	for(var i = 0; i < len; i++){
		if (layers[i].get('name') === layerName ||  layers[i].get('name') === 'nodePx') {
		    //alert(layers[i].get('name'));
			layers[i].setVisible(true);
		} else{
			layers[i].setVisible(false);
		}
	}
}

/**
 * Finds recursively the layer with the specified key and value.
 * @param {ol.layer.Base} layer
 * @param {String} key
 * @param {any} value
 * @returns {ol.layer.Base}
 */
var findBy = function(layer, key, value) {
	if (layer.get(key) === value) {
		return layer;
	}

	// Find recursively if it is a group
	if (layer.getLayers) {
		var layers = layer.getLayers().getArray(),
				len = layers.length, result;
		for (var i = 0; i < len; i++) {
			result = findBy(layers[i], key, value);
			if (result) {
				return result;
			}
		}
	}

	return null;
}

/**
 * Create the layer for showing nodes in specified room.
 * 1. Ajax will get a room list for current seleted background from server.
 * 2. Each feature poccesses a room id property.
 * 3. If we find the same room id in the list as the room id in feature, show it.
 */
var createNodePxLayer = function(item){
  $.ajax({
    url: "room/room_list/",
    type: "POST",
    data: {"bg_name": item},
    success: function(list){
      	var roomList = $.parseJSON(list);

		var imageVectorSource = new ol.source.Vector({
	    	format: new ol.format.GeoJSON()
	  	})

      	for (r in roomList){
      		var featureKeep = nodePxVector.getSource().forEachFeature(function(feature) {
				var pro = feature.getProperties();

				if(roomList[r].fields.room == pro.room_id){

				    if(feature !== undefined){
						imageVectorSource.addFeature(feature);
					}
				}
			});
      	}

      	var imageNodeLayer = findBy(map.getLayerGroup(), "name", "nodePx" + item);

      	// we cannot just create a new layer everytime when user do the switching
      	// so here I remove old and then create a new layer
      	if(imageNodeLayer){
      		map.removeLayer(imageNodeLayer);
      	}

      	imageNodeLayer = new ol.layer.Vector({
		  source: imageVectorSource,
		  style: greenNodeStyle,
		  name: "nodePx" + item //sigle pixel nodes layer
		})
      	map.addLayer(imageNodeLayer);
    }
  });
}

//Create a view in pixel projection for image layers
var imageView = new ol.View({
    projection: pixelProjection,
    center: ol.extent.getCenter([0, 300, 1000, 1800]),
    zoom: 2
});

//Get the selected item, this event happens when we switch the background in dropdown-menu
$('#navbar').on('click', '.switch-bg .dropdown-menu li a', function () {
  	setDefaultPrompt();
  
	//Get selected item's id.
	var item = $(this).attr('id');
	// save current view for removing node
	$(".cur-view").val(item);
	
	if(item === 'mapMenu'){
		//switch to EPSG:4326 projection view for map
	    map.setView(ghView);    

	    //Set OSM layer to visible and others invisible
	    setVisible();
	} else {	
		//switch to pixel projection view 
		map.setView(imageView);  
		//Find if imageLayer is existed already, if yes, we just simply need to make it visible
		var layer = findBy(map.getLayerGroup(), 'name', item);
		
		if(layer == null){ //not existed, so we create a new one
			var imageLayer = createImageLayer(item);
			//try here to see if it works
			map.addLayer(imageLayer);
			
			imageLayer.setVisible(true);
		} else {
		   layer.setVisible(true);
		}

		// show nodes in pixel coordinate
		createNodePxLayer(item);
		//also we need to set other image layers to be invisible.
		setImageLayerVisible(item)
	}
});
	
