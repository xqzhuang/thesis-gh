# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HistoryRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nnumber', models.CharField(max_length=20)),
                ('operation', models.CharField(default=b'unknown', max_length=20)),
                ('battery_number', models.CharField(max_length=20)),
                ('rumber', models.CharField(max_length=20)),
                ('location_top', models.FloatField()),
                ('location_left', models.FloatField()),
                ('location_height', models.FloatField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
