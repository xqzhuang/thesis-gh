from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'home.views.main', name='main'),
    url(r'^node/', include('nodes.urls')),
    url(r'^home/', include('home.urls')),
    url(r'^room/', include('rooms.urls')),
    url(r'^account/', include('accounts.urls')),
]