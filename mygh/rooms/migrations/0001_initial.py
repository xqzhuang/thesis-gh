# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Background',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('bg_type', models.CharField(default=b'JPG', max_length=4, choices=[(b'JPG', b'jpg'), (b'PNG', b'png'), (b'SVG', b'svg')])),
                ('rooms_number', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rnumber', models.CharField(max_length=10)),
                ('width', models.FloatField()),
                ('length', models.FloatField()),
                ('background_name', models.CharField(max_length=20, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='RoomGPSLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('latitute_lt', models.DecimalField(max_digits=18, decimal_places=14)),
                ('longitute_lt', models.DecimalField(max_digits=18, decimal_places=14)),
                ('latitute_lb', models.DecimalField(max_digits=18, decimal_places=14)),
                ('longitute_lb', models.DecimalField(max_digits=18, decimal_places=14)),
                ('room', models.ForeignKey(to='rooms.Room')),
            ],
        ),
        migrations.CreateModel(
            name='RoomPixelLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pixel_x_lt', models.IntegerField()),
                ('pixel_y_lt', models.IntegerField()),
                ('pixel_x_rb', models.IntegerField()),
                ('pixel_y_rb', models.IntegerField()),
                ('room', models.ForeignKey(to='rooms.Room')),
            ],
        ),
        migrations.AddField(
            model_name='background',
            name='room',
            field=models.ForeignKey(default=None, blank=True, to='rooms.Room', null=True),
        ),
    ]
