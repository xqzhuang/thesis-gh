from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .form import AddRoomForm, AddBgForm
from .models import *

# Response a room list in a specific static image background
@csrf_exempt
def room_list(request):
    bg_name = request.POST.get('bg_name', '')

    # query room list by the bg name, could be a list or a single room
    room_list = Background.objects.filter(name = bg_name)

    sroom_list = serializers.serialize('json', room_list, use_natural_foreign_keys=True)
    return JsonResponse(sroom_list, safe=False)   


def add_bg(request):
    if request.method == 'POST':
        form = AddBgForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data  

            background = Background()
            background.name = data['name']
            background.rooms_number = data['rooms_number']
            background.save()

            # redirect to home page
            return HttpResponseRedirect('/')
    else:
        form = AddBgForm()

    return render(request, 'add_bg.html', {'form': form}) 


def add_room(request):
    if request.method == 'POST':
        form = AddRoomForm(request.POST or None)

        if form.is_valid():
            data = form.cleaned_data            

            rnumber = data['rnumber']
            width = data['width']
            length = data['length']

            background_id = data['background']
            background = Background.objects.get(pk = background_id)
            if background is not None:
                rpl = save_pixel_location(data)

            # save and return rgl object    
            rgl = save_gps_location(data)    

            # start to save room 
            room = Room()
            room.rnumber = rnumber
            room.width = width
            room.length = length
            if background is not None:
                room.roompixellocation = rpl
                room.background = background
            room.roomgpslocation = rgl   
            room.save()

 			# redirect to home page
            return HttpResponseRedirect('/')
    else:
        form = AddRoomForm()

    return render(request, 'add_room.html', {'form': form})	

def save_pixel_location(data):
    background_id = data['background']
    background = Background.objects.get(pk = background_id)
    if background is not None:
        rpl = RoomPixelLocation()
        rpl.pixel_x_lt = data['l_t_x']
        rpl.pixel_y_lt = data['l_t_y']
        rpl.pixel_x_rb = data['r_b_x']
        rpl.pixel_y_rb = data['r_b_y'] 
        rpl.save()

        return rpl
    return None

def save_gps_location(data):
    # TODO: use same approach as node to initial room position
    rgl = RoomGPSLocation()
    rgl.latitute_lt = data['l_t_lat']
    rgl.longitute_lt = data['l_t_lon']
    rgl.latitute_lb = data['l_b_lat']
    rgl.longitute_lb = data['l_b_lon']
    rgl.save()

    return rgl