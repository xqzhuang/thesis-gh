from django.db import models
from django.utils.encoding import smart_unicode

# Create your models here.
class Room(models.Model):
	rnumber = models.CharField(max_length=10, unique=False)
	width = models.FloatField()
	length = models.FloatField()
	background_name = models.CharField(max_length=20, null=True, blank=True)
 
	def __unicode__(self):
		return "Room: " + smart_unicode(self.rnumber)

#Background
class Background(models.Model):
	JPG = 'JPG'
	PNG = 'PNG'
	SVG = 'SVG'
	BACKGROUND_TYPE = (
		(JPG, 'jpg'),
		(PNG, 'png'),
		(SVG, 'svg'),
	)
	name = models.CharField(max_length=30)
	bg_type = models.CharField(max_length=4, 
							choices = BACKGROUND_TYPE,
							default = JPG)
	#This field is to show how many rooms in one background picture.
	rooms_number = models.IntegerField()
	room = models.ForeignKey(Room, blank=True, default=None, null=True)

	def __unicode__(self):
		return "Background: " + smart_unicode(self.name)

class RoomGPSLocation(models.Model):
	latitute_lt = models.DecimalField(max_digits=18, decimal_places=14)
	longitute_lt = models.DecimalField(max_digits=18, decimal_places=14)
	latitute_lb = models.DecimalField(max_digits=18, decimal_places=14)
	longitute_lb = models.DecimalField(max_digits=18, decimal_places=14)
	room = models.ForeignKey(Room)
	def __unicode__(self):
		return "RoomGPSLocation Latitute_LT: " + smart_unicode(self.latitute_lt)

class RoomPixelLocation(models.Model):
	pixel_x_lt = models.IntegerField()
	pixel_y_lt = models.IntegerField()
	pixel_x_rb = models.IntegerField()
	pixel_y_rb = models.IntegerField()
	room = models.ForeignKey(Room)
	def __unicode__(self):
		return "RoomPixelLocation. "

