from django.contrib import admin

from .models import *
# Register your models here.
#class SignInAdmin(admin.ModelAdmin):
#	list_display = ['username', 'timestamp'] #what you want to show in admin page
#	class Meta:
#		model = SignIn

#admin.site.register(SignIn, SignInAdmin)

class BackgroundAdmin(admin.ModelAdmin):
	list_display = ['name', 'rooms_number']
	class Meta:
		model = Background

admin.site.register(Background, BackgroundAdmin)

class RoomAdmin(admin.ModelAdmin):
	list_display = ['rnumber', 'width', 'length', 'background_name']
	class Meta:
		model = Room

admin.site.register(Room, RoomAdmin)

class RoomPixelLocationAdmin(admin.ModelAdmin):
	list_display = ['pixel_x_lt', 'pixel_y_lt', 'pixel_x_rb', 'pixel_y_rb', 'room']
	class Meta:
		model = RoomPixelLocation

admin.site.register(RoomPixelLocation, RoomPixelLocationAdmin)

class  RoomGPSLocationAdmin(admin.ModelAdmin):
	list_display = ['latitute_lt', 'longitute_lt', 'latitute_lb', 'longitute_lb', 'room']
	class Meta:
		model = RoomGPSLocation

admin.site.register(RoomGPSLocation, RoomGPSLocationAdmin)
		
			