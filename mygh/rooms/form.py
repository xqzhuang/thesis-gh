from django import forms
from .models import *

class AddBgForm(forms.Form):
	name = forms.CharField(label = 'Name')
	rooms_number = forms.IntegerField(label = 'How many rooms')


def get_background_choices():
	choices_list = [[background.id, background.name] for background in Background.objects.all()]
	
	choices_flag = False

	if len(choices_list) == 0 :
		# print 'list is empty'
		# I don't think it's a good idea :(
		choices_flag = True
		choices_list = [{'', 'No background is available.'}]
	else:
		choices_list.insert(0, {'', '--Choose a background--'})
	
	return choices_list, choices_flag


class AddRoomForm(forms.Form):

	choices_list, choice_flag = get_background_choices()

	rnumber = forms.CharField(label = 'Number')
	width = forms.IntegerField(label = 'Width')
	length = forms.IntegerField(label = 'Length')

	background = forms.ChoiceField(label = 'Background', required = False,
        widget = forms.Select, choices = choices_list, initial = 'None')

	# we need pixel location if user choose a background
	l_t_x = forms.IntegerField(label = 'Left-Top-X')
	l_t_y = forms.IntegerField(label = 'Left-Top-Y')
	r_b_x = forms.IntegerField(label = 'Right-Bottom-X')
	r_b_y = forms.IntegerField(label = 'Right-Bottom-Y')	

	# we need lat/lon for sure
	l_t_lat = forms.DecimalField(label = 'Left-Top-Lat')
	l_t_lon = forms.DecimalField(label = 'Left-Top-Lon')
	l_b_lat = forms.DecimalField(label = 'Left-Bottom-Lat')
	l_b_lon = forms.DecimalField(label = 'Left-Bottom-Lon')

	def __init__(self, *args, **kwargs):
		super(AddRoomForm, self).__init__(*args, **kwargs)
		instance = getattr(self, 'instance', None)

		# Disable select and hide pixel setting when there is no background available
		choices_list, choice_flag = get_background_choices()
		if choice_flag == True:
			# print "disable select"
			self.fields['background'].widget.attrs['disabled'] = True
		
			self.fields['l_t_x'].widget = forms.HiddenInput()
			self.fields['l_t_y'].widget = forms.HiddenInput()
			self.fields['r_b_x'].widget = forms.HiddenInput()
			self.fields['r_b_y'].widget = forms.HiddenInput()

