from django.conf.urls import include, url

urlpatterns = [
    url(r'^add_room/$', 'rooms.views.add_room', name='add_room'),
    url(r'^add_bg/$', 'rooms.views.add_bg', name='add_bg'),
    # provide a room list in a specific static background image
    url(r'^room_list/$', 'rooms.views.room_list', name='room_list'),
]
