from django.shortcuts import render
from .form import UserForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect

def add_user(request):
    if request.method == "POST":
        form = UserForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data
            username = data['username']
            email = data['email']
            password = data['password1']

            new_user = User.objects.create_user(username, email, password)
            return HttpResponseRedirect('/')
    else:
        form = UserForm() 

    return render(request, 'add_user.html', {'form': form}) 