from django.conf.urls import include, url
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^login/$',auth_views.login,name='login',kwargs={'template_name': 'login.html'}),
    url(r'^logout/$',auth_views.logout,name='logout',kwargs={'next_page': '/'}),
    url(r'^add_user/$', 'accounts.views.add_user', name='add_user'),
]
