from django.db import models
from django.utils.encoding import smart_unicode

from rooms.models import Room
from djgeojson.fields import PointField

class SensorBasis(models.Model):
	batt = models.FloatField(null=True)
	temp = models.FloatField(null=True)
	hum = models.FloatField(null=True)
	src = models.IntegerField()
	dst = models.IntegerField()
	received_date = models.DateTimeField()
	remain_life = models.FloatField(null=True, default='400')
	# type:0 - default with longer life
	# type:1 - longer life battery
	# type:2 - shorter life battery
	batt_type = models.IntegerField(default=0)

	def __unicode__(self):
		return "Battery number:" + smart_unicode(self.src)

#Battery table
class Battery(models.Model):
	LOW = 'LOW'
	NORMAL = 'NOR'
	BATTERY_STATUS = (
		(LOW, 'Low'),
		(NORMAL, 'Normal'),
	)
	number = models.CharField(max_length=20, null=False, blank=False, default = 'B232')
	status = models.CharField(max_length=3, 
								choices = BATTERY_STATUS, 
								default = NORMAL)

	def natural_key(self):
		return (self.pk, self.number)

	def __unicode__(self):
		return "Battery number:" + smart_unicode(self.number)

#Nodes real location
class NodeLocation(models.Model):
	top = models.FloatField()
	left = models.FloatField()
	height = models.FloatField()
	room = models.ForeignKey(Room)
 
	def natural_key(self):
		return (self.top, self.left, self.height, self.room.rnumber)

	def __unicode__(self):
		return  "Node Location: room = " + smart_unicode(self.room)

#Nodes
class Node(models.Model):
	number = models.CharField(max_length=20, null=False, blank=False, default = 'N232')
	battery = models.OneToOneField(Battery)
	nodelocation = models.OneToOneField(NodeLocation)

	def __unicode__(self):
		return "Node number: " + smart_unicode(self.number)

#Nodes PixelCoordinate
class PixelCoordinate(models.Model):
	node = models.ForeignKey(Node)
	# This field is only for passing node id throught geojson plugin, I couldn't find another way to do it
	node_number = models.CharField(max_length=20) 
	room_id = models.IntegerField(unique=False)
	geom = PointField()

	def __unicode__(self):
		return "location: x:" + smart_unicode(self.geom)

#Nodes GPSCoordinate
class GPSCoordinate(models.Model):
	node = models.OneToOneField(Node)
	node_number = models.CharField(max_length=20)
	geom = PointField()

	def __unicode__(self):
		return "Node GPS: " + smart_unicode(self.geom)
