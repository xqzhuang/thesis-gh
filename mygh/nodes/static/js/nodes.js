///////////////////////////////////////////////////////////////////////////////////////////////////
//
//Some constance objects
//
///////////////////////////////////////////////////////////////////////////////////////////////////
//Style for marker with node is dead(stop sending packet)
var blackNodeStyle = [
  new ol.style.Style({
    image: new ol.style.Circle({
      radius: 4,
      fill: new ol.style.Fill({
        color: '#000000'
      })
    })
  })
];

//Style for marker with node is in low battery
var yellowNodeStyle = [
  new ol.style.Style({
    image: new ol.style.Circle({
      radius: 4,
      fill: new ol.style.Fill({color: '#DAA520'}),
      stroke: new ol.style.Stroke({color: 'rgba(255,127,14,1)', width: 1})
    })
  })
];

//Style for marker with node is going to die
var redNodeStyle = [
  new ol.style.Style({
    image: new ol.style.Circle({
      radius: 5,
      fill: new ol.style.Fill({color: '#FF0000'}),
      stroke: new ol.style.Stroke({color: 'rgba(214,39,40,1)', width: 2})
    })
  })
];

//Style for marker with node works in normal
var greenNodeStyle = [
  new ol.style.Style({
    image: new ol.style.Circle({
      radius: 4,
      fill: new ol.style.Fill({color: '#3CB371'}),
      stroke: new ol.style.Stroke({color: 'rgba(38, 114, 82, 1)', width: 1})
    })
  })
];

var defSHTML = "No sensor is selected.";
var defNHTML = "No node is selected.";

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//Nodes feature layer, rendering as markers in map.
//
///////////////////////////////////////////////////////////////////////////////////////////////////
//GPS location nodes goes here
var nodeVector = new ol.layer.Vector({
  source: new ol.source.GeoJSON({
    projection : map.getView().getProjection(),
    url: 'node/gps.geojson' 
  }),
  name: 'node',
  style: greenNodeStyle
});

//We add GPS location nodes by default, since default view is map.
map.addLayer(nodeVector);

//Pixel location nodes goes here, here just create the layer, but don't add to the map.
//Later process will filter the feature according to current selected view.
var nodePxVector = new ol.layer.Vector({
  source: new ol.source.GeoJSON({
    url: 'node/pixel.geojson' 
  }),
  projection : map.getView().getProjection(), //get current view's projection
  name: 'nodePx',
  style: greenNodeStyle
});

///////////////////////////////////////////////////////////////////////////////////////////////////
//
// Adding propertities for nodes which existed in the layer.
// Because in the intial loading, we only had location information
// This is not a very good approach to perform it though. 
//
///////////////////////////////////////////////////////////////////////////////////////////////////
// Adding sensor properties and update it
var getSensorPro = function(){
  $.ajax({
    url: "node/node_properties",
    type: "GET",
    success: function(pros){
      var json_obj = $.parseJSON(pros);

      for (var i in json_obj){

        var pro = json_obj[i];
        
        var src  = pro.fields.src;
        var batt = pro.fields.batt;
        var time = pro.fields.received_date;

        // TODO: add a function for this duplicate code
        // retrieve all features in this layer
        var featuresGps = nodeVector.getSource().getFeatures();
        featuresGps.forEach(function (f) {
            // add properties and change style if we find matching node
            if(f.getProperties().node_number.substring(1) == src){

              var status = changeStyle(f, batt, time);
              f.setProperties({'status': status})
              // adding sensor properties
              f.setProperties(json_obj[i]);
          }
        })

        var featuresPixel = nodePxVector.getSource().getFeatures();
        featuresPixel.forEach(function (f) {
            // add properties and change style if we find matching node
            if(f.getProperties().node_number.substring(1) == src){

              var status = changeStyle(f, batt, time);
              f.setProperties({'status': status})
              // adding sensor properties
              f.setProperties(json_obj[i]);
          }
        })
      } 
    }
  });
}

// execute first time without delay
getSensorPro();
// update it every 4s.
setInterval(getSensorPro, 4000);

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//Feature style, change style according to battery level and last received time
//
///////////////////////////////////////////////////////////////////////////////////////////////////
var changeStyle = function(feature, batt, time){
  var status = "Normal";
  // compare current time with the time query in database
  // how long we can determine it to be dead?
  var curTime = new Date($.now());
  var recentTime = new Date(time)

  // hour difference
  var diff = (curTime - recentTime) /1000 / 60 /60; 

  // we haven't received any packet from this node in the past 48? hours, most likely this node is dead
  if(diff >= 48){
    feature.setStyle(blackNodeStyle);
    status = "Dead";     
    return status;
  }

  if(batt > 2.4){
    // normal, green. this is default style, do nothing right now

  } 
  else if(batt >= 2.2 && batt <= 2.4){
    // yellow, low
    feature.setStyle(yellowNodeStyle);
    status = "Low"; 
  }
  else if(batt < 2.2){
    // red, danger
    feature.setStyle(redNodeStyle);
    status = "Danger";
  }

  return status;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//Popup and click event
//
///////////////////////////////////////////////////////////////////////////////////////////////////
// get value from database
var getNodeAjax = function(handleData){
  $.ajax({
    url : "node/node_info",
    type: "GET",
    success: function(info)
    {
       handleData(info);    
    },
    error: function (info)
    {
       //TODO: some prompt for error?
    }
  });
} 

var showNodeLocation = function(node_number){
    var node;
    getNodeAjax(function(info){
      var json_obj = $.parseJSON(info);

      for (var i in json_obj){
        var number = json_obj[i].fields.number; //skip first letter

        if(node_number == number){
           node = json_obj[i];
      }
    }

    // pass node object to modify page
    sessvars.node = {number:node_number, 
      id: node.pk, 
      room: node.fields.nodelocation[3],
      batt: node.fields.battery, 
      top: node.fields.nodelocation[0], 
      left: node.fields.nodelocation[1], 
      height: node.fields.nodelocation[2]};
    
    // display node information
    var shtml = "<ul class='list-group'><li class='list-group-item'><b>Battery:</b><span class='badge'>"+ node.fields.battery[1]  + 
        "</span></li><li class='list-group-item'><b>Room:</b> <span class='badge'>" + node.fields.nodelocation[3] + 
        "</span></li><li class='list-group-item'><b>Top:</b> <span class='badge'>" + node.fields.nodelocation[0] +  "m" + 
        "</span></li><li class='list-group-item'><b>Left:</b> <span class='badge'>" + node.fields.nodelocation[1] +  "m" + 
        "</span></li><li class='list-group-item'><b>Height:</b> <span class='badge'>" + node.fields.nodelocation[2] + "m</li></ul>";

    $(".node-location").html(shtml);
  });
}

var parseIsoDatetime = function(dtstr) {
    var dt = dtstr.split(/[: T-]/).map(parseFloat);
    console.log("convert!" + dt);
    return new Date(dt[0], dt[1] - 1, dt[2], dt[3] || 0, dt[4] || 0, dt[5] || 0, 0);
}

// Click feature
map.on('click', function(evt) {
 // get the closest feature to the event point
  var feature = map.forEachFeatureAtPixel(evt.pixel,
    function(feature, layer) {
    return feature;
  });

  showNodeInfo(feature);
});

var showNodeInfo = function(feature){
  if (feature) {
  var geometry = feature.getGeometry();
  
  if(geometry.getType() == 'Point') { 

      var pro = feature.getProperties();
      // MUST make sure os is in the correct timezone
      var time = new Date(pro.fields.received_date).toLocaleString();
     
      var batt = 0;
      var temp = 0;
      var hum = 0;
      if(pro.fields.batt){
        batt = pro.fields.batt.toFixed(2);
      }

      if(pro.fields.temp){
        temp = pro.fields.temp.toFixed(2);
      }

      if(pro.fields.hum){
        hum = pro.fields.hum.toFixed(2);
      }

      var options = {timeZone: "Europe/London"};
      // display sensor information
      var shtml = "<ul class='list-group'><li class='list-group-item'><b>Number:</b> <span class='badge'>"+ pro.node_number  +
            "</span></li><li class='list-group-item'><b>Temperature:</b><span class='badge'>" + temp +
            "&deg</span></li><li class='list-group-item'><b>Humidity:</b><span class='badge'>" + hum +
            "HR%</span></li><li class='list-group-item'><b>Battery:</b><span class='badge'>" + batt +
            "V</span><a role='button' class='view-chart'>View Chart</a></li><li class='list-group-item'><b>Time:</b><span class='badge'>" + time +
            "</li></ul>";

      $(".sensor-basis").html(shtml);
      $(".btn-modify").show();
      $(".btn-remove-node").show();

      // show node location
      showNodeLocation(pro.node_number);
    } 
    else if(geometry.getType() == 'Polygon') {
      console.log('You clicked polygon!');          
    }
  }

}

var popup = new ol.Overlay.Popup();
map.addOverlay(popup);

// Popup while mouse move over marker
map.on('pointermove', function(evt) {
  if (evt.dragging) {
    return;
  }

  // get the closest feature to the event point
  var feature = map.forEachFeatureAtPixel(evt.pixel,
    function(feature, layer) {
    return feature;
    });
  
  showPopup(feature);
});

var showPopup = function(feature) {
  // show the popup
  if (feature) {
  var geometry = feature.getGeometry();

  if(geometry.getType() == 'Point') { //only popup when we click markers

    var coord = geometry.getCoordinates();    
    var pro = feature.getProperties();
    var status = pro.status;
    var statusHtml = 'Loading...';

    var batt = 0;
    var temp = 0;
    var hum = 0;
    var life = 'unknown';

    if(pro.fields != undefined){          
      if(pro.fields.batt){
        batt = pro.fields.batt.toFixed(2);
      }

      if(pro.fields.temp){
        temp = pro.fields.temp.toFixed(2);
      }

      if(pro.fields.hum){
        hum = pro.fields.hum.toFixed(2);
      }

      if(pro.fields.remain_life){
        hour = pro.fields.remain_life;

        day = hour / 24;
        left_hour = Math.floor(hour) % 24;
        life = 'About ' + Math.floor(day) + ' days and ' + left_hour + ' hours left';
      }
    }

    if(status == 'Normal'){
      statusHtml = '<div class="alert alert-success batt-status" role="alert">Battery works well</br>'+ life +'</div>';
    } 
    else if(status == 'Low'){
      statusHtml = '<div class="alert alert-warning batt-status" role="alert">Warning!Battery is low</br>'+ life +'</div>';
    }
    else if(status == 'Danger'){
      statusHtml = '<div class="alert alert-danger batt-status" role="alert">Danger!Battery is going to die!</br>'+ life +'</div>';
    }else if(status == 'Dead'){
      statusHtml = '<div class="alert batt-status" role="alert">This node is propably dead.</div>';
    }

    popup.show(coord, '<div class="node-popup"><p><b>Node:</b>' + pro.node_number + '</p> <p> <b>Battery:</b>' 
      + batt + 'V</p> <p><b>Temperature:</b>' 
      + temp + '&deg</p> <p> <b>Humidity:</b>' 
      + hum + 'HR%</p>'
      + statusHtml +'</div>'); 

    // save node coordinate id(here same as feature id), we will need it while removing node.
    $('.cor-id').val(feature.getId());
    } 
    else {
      popup.hide();
    } 
  } 
  else {
    popup.hide();
  }
}

// Remove a node
$(".btn-remove-node").click(function(){
  var corId = $(".cor-id").val();
  if(!corId){
    alert("Please select a node!");
    return;
  }

  var answer = confirm("Are you sure you want to remove this node?");
  if(!answer){
    return;
  }

  var curView = $(".cur-view").val();

  var feature;
  if(curView == "mapMenu"){
    feature = nodeVector.getSource().getFeatureById(corId);
  }
  else {
    feature = nodePxVector.getSource().getFeatureById(corId);
  }

  $.ajax({
    url : "node/delete_node/",
    type: "POST",
    data: {"cor_id": corId, "cur_view": curView},
    success: function(info){
      alert("Success!");
      
      // set default prompts back
      setDefaultPrompt();
    },
    error: function (info){
      alert("There was some problem while removing node!");
    }
  });  
  
  updateSource(curView, feature);
});

var updateSource = function(curView, feature){
  removeFeature(feature, nodePxVector);

  // if we are in map view, remove the feature directly
  if(curView == "mapMenu"){
    nodeVector.getSource().removeFeature(feature);
  }
  else{ // if we are in a image view, find the feature with same node number (src)
    removeFeature(feature, nodeVector);
  }

  createNodePxLayer(curView);
}

var removeFeature = function(feature, vector){
  vector.getSource().forEachFeature(function(f) {
    var pro = f.getProperties();

    if(feature.getProperties().fields.src == pro.fields.src){
      console.log("found match feature");
        if(f !== undefined){
         vector.getSource().removeFeature(f);
      }
    }
  });
}

// Default information for node
var setDefaultPrompt = function(){
  $(".node-location").html(defNHTML);
  $(".sensor-basis").html(defSHTML);
  $(".node-modification").hide();
  $(".btn-modify").hide();
  $(".btn-remove-node").hide();
  $(".cor-id").val("");
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//
//Get node id from url. This is needed while user click map view from node list page.
//
///////////////////////////////////////////////////////////////////////////////////////////////////
$( document ).ready(function() {
  var qs = (function(a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
        var p=a[i].split('=', 2);
        if (p.length == 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
  })(window.location.search.substr(1).split('&'));

  setTimeout(
    function() 
    {
      // parse url to get src sent from node page.
      var src = qs["src"];
      var featureGps = nodeVector.getSource().forEachFeature(function(feature) {
        var pro = feature.getProperties();
        if(pro.fields == undefined){
            return;
        }

        if(pro.fields.src == src.substring(1))
        {
          return feature
        }

      });      

      showPopup(featureGps);  
      showNodeInfo(featureGps);     
    }, 2000);
});
