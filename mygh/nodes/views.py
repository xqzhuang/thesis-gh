from django.shortcuts import render, render_to_response, RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from django.db.models import Max, Count
from django.views.decorators.csrf import csrf_exempt

from django.core import serializers
from djgeojson.serializers import Serializer as GeoJSONSerializer
from .form import AddNodeForm, AddBatteryForm, ModifyNodeForm
from .models import *
from rooms.models import *
from home.models import *

import utm
import math
import json

def modify_node(request):
    if request.method == 'POST':
        form = ModifyNodeForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data

            node_id = data['node_id']
            battery_id = data['battery']
            room_number = data['room']
            top = data['top']
            left = data['left']
            height = data['height']

            node = Node.objects.get(pk = node_id)
            # recaculate coordinate
            gps_cor = GPSCoordinate.objects.get(node = node)
            pixel_cor = PixelCoordinate.objects.filter(node = node)

            room = Room.objects.filter(rnumber = room_number)
            battery = Battery.objects.get(pk = battery_id)
            node.battery = battery

            nodelocation = NodeLocation.objects.get(pk = node.nodelocation.pk)
            nodelocation.top = top
            nodelocation.left = left
            nodelocation.height = height
            nodelocation.room = room[0]
            nodelocation.save()

            node.nodelocation = nodelocation
            node.save()

            cal_gps_location(room[0], node, gps_cor)
            cal_pixel_location(room[0], node, pixel_cor)

            history = HistoryRecord()
            history.nnumber = node.number
            history.operation = 'Modify'
            history.battery_number = battery.number
            history.rumber = room[0].rnumber
            history.location_top = top
            history.location_left = left
            history.location_height = height
            history.save()

            return HttpResponseRedirect('/')
    else:
        form = ModifyNodeForm()

    return render(request, 'modify_node.html', {'form': form})

# node list in each background
@csrf_exempt
def node_list_pixel(request):
	bg_name = request.POST.get('bg_name', '')

	# query room list by the bg name, could be a list
	rooms = Background.objects.filter(name = bg_name)

	node_list_pixel = []
	for room in rooms:
		nodes = PixelCoordinate.objects.filter(room_id = room.pk)
		node_list_pixel.append(nodes)
	
	snode_list_pixel = GeoJSONSerializer().serialize(node_list_pixel, use_natural_keys=True, with_modelname=False)
	return JsonResponse(snode_list_pixel, safe=False)	

@csrf_exempt
def delete_node(request):
    cor_id = request.POST.get('cor_id', '')
    cur_view = request.POST.get('cur_view', '')

    if cur_view == 'mapMenu':
    	gps_cor = GPSCoordinate.objects.get(pk=cor_id)
    	node_id = gps_cor.node.pk
    else:
    	pixel_cor = PixelCoordinate.objects.get(pk=cor_id)
    	node_id = pixel_cor.node.pk
    
    node = Node.objects.get(pk=node_id);
    battery = Battery.objects.get(pk=node.battery.pk)
    room = Room.objects.get(pk=node.nodelocation.room_id) 

    history = HistoryRecord()
    history.nnumber = node.number
    history.operation = 'Delete'
    history.battery_number = battery.number
    history.rumber = room.rnumber
    history.location_top = node.nodelocation.top
    history.location_left = node.nodelocation.left
    history.location_height = node.nodelocation.height
    history.save()

    node.delete()
    node.nodelocation.delete()

    return HttpResponseRedirect('/')

@csrf_exempt
def node_info(request):
    nodes = Node.objects.all()
    snodes = serializers.serialize('json', nodes, use_natural_foreign_keys=True)

    return JsonResponse(snodes, safe=False)	

def node_properties(request):
	last = last_basis()
	# print last
	s = serializers.serialize('json', last)

	return JsonResponse(s, safe=False)

def last_basis():
	# I'm sure there is a better approach to do it...
	# query sensor group by node src.
	nodes = SensorBasis.objects.values('src').annotate(scount=Count('src'))
	
	# list with most recent received packet for each node.
	last = []
	for n in nodes:
		# get node id
		nid = n['src']
		# queryset with lastest time, we only need information from last packet
		l = SensorBasis.objects.filter(src=nid).aggregate(Max('received_date'))
		# instance which filtered by src and most recent received time 
		v = SensorBasis.objects.get(src=nid, received_date = l['received_date__max'])
		# write every instance into list
		last.append(v)

	return last	

def add_batt(request):
    if request.method == 'POST':
        form = AddBatteryForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data

            battery = Battery()
            battery.number = data['number']
            battery.save()
 			# redirect to home page
            return HttpResponseRedirect('/')
    else:
        form = AddBatteryForm()

    return render(request, 'add_batt.html', {'form': form})	

def node(request):
	nodes = Node.objects.all()

	nodes_data = {
		"node_detail" : nodes
	}

	return render_to_response("node.html", 
							nodes_data,
							context_instance=RequestContext(request))

def get_nodes_cor(request):
	# select data from location and basis
	geo = serializers.serialize('json', NodeLocation.objects.all())

	return JsonResponse(geo, safe=False)

def add_node(request):
    if request.method == 'POST':
        form = AddNodeForm(request.POST or None)
        if form.is_valid():
            data = form.cleaned_data

            number = data['number']
            room_number = data['room']
            battery_id = data['battery']

            top = data['top']         
            left = data['left']
            height = data['height']

            # query room object
            room = Room.objects.filter(rnumber = room_number)
           
            # save nodeLocation
            nodeLocation = NodeLocation()
            nodeLocation.top = top
            nodeLocation.left = left
            nodeLocation.height = height
            nodeLocation.room = room[0]
            nodeLocation.save()

            # save node
            node = Node()
            battery = Battery.objects.get(pk = battery_id)
            node.battery = battery
            node.number = number
            node.nodelocation = nodeLocation
            node.save()
            
            # calculate and save gpslocation
            cal_gps_location(room[0], node, None)

            # calculate and save pixellocation
            cal_pixel_location(room[0], node, None)

            history = HistoryRecord()
            history.nnumber = number
            history.operation = 'Add'
            history.battery_number = battery.number
            history.rumber = room[0].rnumber
            history.location_top = top
            history.location_left = left
            history.location_height = height
            history.save()

 			# redirect to home page
            return HttpResponseRedirect('/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = AddNodeForm()

    return render(request, 'add_node.html', {'form': form})	

# Calculate node's pixel coordinate and save to database
# one node may has several pixel coordinates depends on image background
def cal_pixel_location(room, node, pixel_cor):
	top = node.nodelocation.top
	left = node.nodelocation.left

	# room's size
	width = room.width
	length = room.length

	# room list with the same number as the one user chose
	rooms = Room.objects.filter(rnumber = room.rnumber)

	# make a pixel coordinate list by using the room id 
	# query all the instances first
	roompixellocations = RoomPixelLocation.objects.all()

	pixelList = []
	for plocation in roompixellocations:
		for r in rooms:
			if r.pk == plocation.room.pk:
				# add to a list
				pixelList.append(plocation)

	# room's pixel location in current image
	# roompixellocation = RoomPixelLocation.objects.get(room = room.pk)
	i = 0
	for pl in pixelList:
		print 'room id=' + str(pl.room_id)
		# left-top coordinate
		pixel_x_lt = pl.pixel_x_lt
		pixel_y_lt = pl.pixel_y_lt

		# right-bottom coordinate
		pixel_x_rb = pl.pixel_x_rb
		pixel_y_rb = pl.pixel_y_rb

		# now we have enough information for calculating the node's pixel coordinate in current imgae
		# formular: x = RoomLT_X + node_left / room_real_width * room_pixel_width
		#          y = RoomLT_Y - node_top / room_real_length * room_pixel_length
		x = pixel_x_lt + float(left) / width * math.fabs(pixel_x_rb - pixel_x_lt)
		y = pixel_y_lt - float(top) / length * math.fabs(pixel_y_rb - pixel_y_lt)
        
		if pixel_cor is None:
			pixelLocation = PixelCoordinate()
		else:
			pixelLocation = pixel_cor[i]

		i += 1
		pixelLocation.node = node
		pixelLocation.geom = {'type': 'Point', 'coordinates': [x, y]}
		pixelLocation.room_id = pl.room.pk
		pixelLocation.node_number = node.number
		pixelLocation.save()

# Calculate node's gps coordinate
# 1. Calculate node's UTM coordinate by using relative parameters
# 2. Transform it to Geographic coordinate. (Latitude/longitude
def cal_gps_location(room, node, gps_cor):
	top = node.nodelocation.top
	left = node.nodelocation.left

	roomgpslocation	= RoomGPSLocation.objects.get(room = room.pk)
	#TODO: raise an execption here

	# room's gps location, we only need two points
	# left-top coordinate
	latitute_lt = roomgpslocation.latitute_lt
	longitute_lt = roomgpslocation.longitute_lt

	# left-bottom coordinate
	latitute_lb = roomgpslocation.latitute_lb
	longitute_lb = roomgpslocation.longitute_lb

	# room left-top UTM coordinate
	# I don't realy care about zone number and letter, since we just in very short distance, keep them just in case.
	# (EASTING, NORTHING, ZONE NUMBER, ZONE LETTER).
	rxt, ryt, zn, zl = geo_to_utm(longitute_lt, latitute_lt)
	# Room left-bottom UTM coordinate
	rxb, ryb, zn, zl = geo_to_utm(longitute_lb, latitute_lb)

	# Let's start with calculating room's coordinate
	a = math.fabs(rxb - rxt)
	b = math.fabs(ryb - ryt)

	# convert node's real location to float
	fl = float(left)
	ft = float(top)

	# d = degree
	# Maybe I don't need to convert to degree to do the calculation. Because I will need to convert it back eventually
	dx = math.degrees(math.atan(b/a))
	dy = math.degrees(math.atan(fl/ft))

	# Hypotenuse of node's top and left
	hn = math.sqrt(ft*ft + fl*fl)

	# we found the delta degree
	dnx =  math.fabs(dx - dy)

	# calculate node's relatively coordinate in meter
	n_x = hn * math.cos(math.radians(dnx))
	n_y = hn * math.sin(math.radians(dnx))

	# alright, let's calculate final result
	# we use room's left-top coordinate as datum point
	if dx > dy:
		node_x = rxt + n_x
		node_y = ryt - n_y
	else:
		node_x = rxt + n_x
		node_y = ryt + n_y

	# now we can transform from UTM to latitude/longitute
	node_lat, node_lon = utm_to_geo(node_x, node_y, zn, zl)

	if gps_cor is None:
		gpsLocation = GPSCoordinate() #insert
	else: 
		gpsLocation = gps_cor  #update

	gpsLocation.node = node
	gpsLocation.node_number = node.number
	gpsLocation.geom = {'type': 'Point', 'coordinates': [node_lon, node_lat]}
	gpsLocation.save()

# Geographic coordinate to UTM, logitute/latitute to x/y in meter
def geo_to_utm(log, lat):
	# (EASTING, NORTHING, ZONE NUMBER, ZONE LETTER).
	x, y, zn, zl = utm.from_latlon(lat, log)
	return (x, y, zn, zl)

def utm_to_geo(x, y, zn, zl):
	lat, log = utm.to_latlon(x, y, zn, zl)
	return (lat, log)