from django.contrib import admin
from .models import *

#SensorBasis
class SensorBasisAdmin(admin.ModelAdmin):
	list_display = ['src', 'dst', 'batt', 'hum', 'temp', 'received_date']
	class Meta:
		model = SensorBasis

admin.site.register(SensorBasis, SensorBasisAdmin)

#Node
class NodeAdmin(admin.ModelAdmin):
	list_display = ['id', 'number', 'battery', 'nodelocation']
	class Meta:
		model = Node

admin.site.register(Node, NodeAdmin)

#Battery
class BatteryAdmin(admin.ModelAdmin):
	list_display = ['number', 'status']
	class Meta:
		model = Battery

admin.site.register(Battery, BatteryAdmin)


#PixelCoordinate
class PixelCoordinateAdmin(admin.ModelAdmin):
	list_display = ['id', 'geom']
	class Meta:
		model = PixelCoordinate

admin.site.register(PixelCoordinate, PixelCoordinateAdmin)

#GPSCoordinate
class GPSCoordinateAdmin(admin.ModelAdmin):
	list_display = ['id', 'geom']
	class Meta:
		model = GPSCoordinate

admin.site.register(GPSCoordinate, GPSCoordinateAdmin)

#NodeLocation
class NodeLocationAdmin(admin.ModelAdmin):
	list_display = ['room', 'top', 'left', 'height']
	class Meta:
		model = NodeLocation

admin.site.register(NodeLocation, NodeLocationAdmin)
