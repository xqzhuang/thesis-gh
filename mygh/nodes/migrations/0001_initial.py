# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djgeojson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Battery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(default=b'B232', max_length=20)),
                ('status', models.CharField(default=b'NOR', max_length=3, choices=[(b'LOW', b'Low'), (b'NOR', b'Normal')])),
            ],
        ),
        migrations.CreateModel(
            name='GPSCoordinate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('node_number', models.CharField(max_length=20)),
                ('geom', djgeojson.fields.PointField()),
            ],
        ),
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(default=b'N232', max_length=20)),
                ('battery', models.OneToOneField(to='nodes.Battery')),
            ],
        ),
        migrations.CreateModel(
            name='NodeLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('top', models.FloatField()),
                ('left', models.FloatField()),
                ('height', models.FloatField()),
                ('room', models.ForeignKey(to='rooms.Room')),
            ],
        ),
        migrations.CreateModel(
            name='PixelCoordinate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('node_number', models.CharField(max_length=20)),
                ('room_id', models.IntegerField()),
                ('geom', djgeojson.fields.PointField()),
                ('node', models.ForeignKey(to='nodes.Node')),
            ],
        ),
        migrations.CreateModel(
            name='SensorBasis',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('batt', models.FloatField(null=True)),
                ('temp', models.FloatField(null=True)),
                ('hum', models.FloatField(null=True)),
                ('src', models.IntegerField()),
                ('dst', models.IntegerField()),
                ('received_date', models.DateTimeField()),
            ],
        ),
        migrations.AddField(
            model_name='node',
            name='nodelocation',
            field=models.OneToOneField(to='nodes.NodeLocation'),
        ),
        migrations.AddField(
            model_name='gpscoordinate',
            name='node',
            field=models.OneToOneField(to='nodes.Node'),
        ),
    ]
