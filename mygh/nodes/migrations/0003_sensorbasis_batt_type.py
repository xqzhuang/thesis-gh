# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nodes', '0002_sensorbasis_remain_life'),
    ]

    operations = [
        migrations.AddField(
            model_name='sensorbasis',
            name='batt_type',
            field=models.IntegerField(default=0),
        ),
    ]
