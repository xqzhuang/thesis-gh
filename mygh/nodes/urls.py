from django.conf.urls import include, url


from djgeojson.views import GeoJSONLayerView
from .models import GPSCoordinate, PixelCoordinate

urlpatterns = [
	url(r'^$', 'nodes.views.node', name='node'),
	url(r'^add_node/$', 'nodes.views.add_node', name='add_node'),
	url(r'^delete_node/$','nodes.views.delete_node', name='delete_node'),
	url(r'^modify_node/$', 'nodes.views.modify_node', name='modify_node'),
	url(r'^add_batt/$', 'nodes.views.add_batt', name='add_batt'),
	# sensor basis
    url(r'^node_properties/$', 'nodes.views.node_properties', name='node_properties'),

    # node's real position
    url(r'^node_info/$', 'nodes.views.node_info', name='node_info'),
    url(r'^node_list_pixel/$', 'nodes.views.node_list_pixel', name='node_list_pixel'),  

    # node coordinate by using geojson
    url(r'^gps.geojson$', GeoJSONLayerView.as_view(model=GPSCoordinate, properties=('node_number'),), name='data'),
    url(r'^pixel.geojson$', GeoJSONLayerView.as_view(model=PixelCoordinate, properties=('node_number', 'room_id'),), name='pixel'),
]
