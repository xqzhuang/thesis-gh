from django import forms
from .models import *
from rooms.models import Room
from django.db import connection

#Query from database for room choice
def get_room_choices():
	room_list = Room.objects.values('rnumber').distinct()

	choices_list = [[room['rnumber'], room['rnumber']] for room in room_list]

	if len(choices_list) == 0 :
		choices_list = [{'', 'No room is available.'}]

	return choices_list

#Query from database for battery choice
def get_unused_battery_choices():
	battery_in_use = Node.objects.values_list('battery', flat=True)
	choices_list = [[battery.id, battery.number] for battery in Battery.objects.exclude(id__in = battery_in_use)]
	
	if len(choices_list) == 0 :
		choices_list = [{'', 'No battery is available.'}]

	return choices_list

# Modify node needs current selected node, I don't know how to inlcude this node in the unused battery choices.
# So instead of passing unused battery, I pass all the battery here, but filter them in front end.
def get_all_battery_choices():
	choices_list = [[battery.id, battery.number] for battery in Battery.objects.all()]
	if len(choices_list) == 0 :
		choices_list = [{'', 'No battery is available.'}]

	return choices_list

class AddNodeForm(forms.Form):
	number = forms.CharField(label = 'Number')
	room = forms.ChoiceField(label = 'Room', required = False,
        widget = forms.Select, choices = get_room_choices())

	battery = forms.ChoiceField(label = 'Battery', required = False,
        widget = forms.Select, choices = get_unused_battery_choices())

	top = forms.CharField(label = 'Top')
	left = forms.CharField(label = 'Left')
	height = forms.CharField(label = 'Height')

	def __init__(self, *args, **kwargs):
		super(AddNodeForm, self).__init__(*args, **kwargs)
		self.fields['room'].choices = get_room_choices()
		self.fields['battery'].choices = get_unused_battery_choices()

class ModifyNodeForm(forms.Form):
	node_id = forms.IntegerField(widget=forms.HiddenInput(), required = False)
	room = forms.ChoiceField(label = 'Room', required = False,
        widget = forms.Select, choices = get_room_choices())

	battery = forms.ChoiceField(label = 'Battery', required = False,
        widget = forms.Select, choices = get_all_battery_choices())

	top = forms.CharField(label = 'Top')
	left = forms.CharField(label = 'Left')
	height = forms.CharField(label = 'Height')

	def __init__(self, *args, **kwargs):
		super(ModifyNodeForm, self).__init__(*args, **kwargs)
		self.fields['room'].choices = get_room_choices()
		self.fields['battery'].choices = get_all_battery_choices()


class AddBatteryForm(forms.Form):
	number = forms.CharField(label = 'Number')