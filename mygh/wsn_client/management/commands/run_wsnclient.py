from django.core.management.base import BaseCommand, CommandError

#!/usr/bin/python
import logging
import argparse
import sys
import datetime
import numpy as np
from subprocess import Popen, PIPE

from wsn_client.wsn.handlers import BaseHandler
from wsn_client.wsn.clients import WSNClient
from wsn_client.wsn.types import WSNTypes
from nodes.models import Node, SensorBasis
from .battery import BatteryUtil

class Command(BaseCommand):

    help = 'start wsn client'

    def handle(self, *args, **options):
        # logging.basicConfig(level=logging.DEBUG, format='%(name)s: %(message)s')

        # parser = argparse.ArgumentParser()
        # subparsers = parser.add_subparsers()

        # # parse start parm
        # start_parser = subparsers.add_parser('start')
        # start_parser.add_argument('--wsn_host', default='plonk.ict.pwr.wroc.pl',help='WSN Server host.')
        # start_parser.add_argument('--wsn_port', type=int, default=5001, help='WSN Server port.')
        # start_parser.set_defaults(function=start_client)

        # args = parser.parse_args(sys.argv[1:])
        # args.function(args)
        self.start_client()

    def start_client(self):
        print 'start client'

        wsnclient = WSNClient()
        wsnclient.connect(host='plonk.ict.pwr.wroc.pl', port=5001)

        wsnclient.register_pattern(
            handler_class = DatabaseHandler,
            agent_name = 'Message with all data in it'
        )   

        wsnclient.forever()

class SimpleHandler(BaseHandler):

    def initialize(self):
        pass

    def handle_message(self, message):
        # print "sensor #%d battery level is %.2f" % (message['src'], message['batt'])
        pass

class DatabaseHandler(BaseHandler):

    def initialize(self):
        """
        Initialize database.
        """
        pass

    def handle_message(self, message):
        src = message['src']
        ts = message['ts']
        dst = message['dst']

        # in case there is not battery info in package
        hum = batt = temp =0

        if 'hum' in message:
            hum = message['hum']

        if 'batt' in message:
            batt = message['batt']

        if 'temp' in message:
            temp = message['temp']

        util = BatteryUtil()

        batt_type = 0
        # lastest instance in database
        lastest = util.lastest_basis(src)

        if lastest is not None:
            batt_type = lastest.batt_type

        # assign a default value, we cannot determine life at the beginning
        remain_life = util.remain_life(batt_type, batt)

        # if battery reaches first point with big varying 
        if batt <= util.VARY and batt >= util.LOWEST:
            print 'battery level less than 2.5 --> ' + str(src)
            # query from database, calculate the time span then determine this node belongs to which trend
            last_high = util.last_high_basis(src)

            # if battery type is not 0, it means we have set this battery type already, don't need to do it again
            if last_high is not None and batt_type == 0:
                time = last_high.received_date
                timedelta = ts - time.replace(tzinfo=None)
                span_cur = timedelta.total_seconds() / 3600
                print span_cur

                span_trend1 = util.solver_trend1(util.VARY) - util.solver_trend1(util.HIGHEST)
                print span_trend1

                span_trend2 = util.solver_trend2(util.VARY) - util.solver_trend2(util.HIGHEST) 
                print span_trend2

                # need to verify
                if span_cur >= span_trend1 or span_trend1 - span_cur < 200:
                    batt_type = 1 # longer life
                elif span_trend1 - span_cur >= 200 or span_trend1 <= span_trend2:
                    batt_type = 2 # shorter life

                print 'battery type' + str(batt_type)
                remain_life = util.remain_life(batt_type, batt)

        # coefficients = (-1.63757809e-09, 2.45878017e-06, -1.30699122e-03, 2.80324849e+00)
        # polynomial = np.poly1d(coefficients)        
        # b = np.polyval(polynomial, 800)

        # simply use django ORM to save into database
        sensor = SensorBasis()
        sensor.batt = batt
        sensor.hum = hum
        sensor.temp = temp
        sensor.src = src
        sensor.dst = dst
        sensor.received_date = ts
        sensor.remain_life = remain_life
        sensor.batt_type = batt_type
        sensor.save()