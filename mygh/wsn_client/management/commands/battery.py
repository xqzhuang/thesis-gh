# -*- coding: utf-8 -*- 
from nodes.models import SensorBasis
from django.db.models import Max, Count
from sympy.solvers import solve
from sympy import Symbol

class BatteryUtil(object):
    # highest voltage
    HIGHEST = 2.8
    # lowest voltage
    LOWEST = 1.8
    # first point of starting big varying
    VARY = 2.5

    # There are 2? types of batteries, each type of battery poccesses different fit function
    # solve polynomial 1, input is battery voltage, output is time in hour
    def solver_trend1(self, deviation):
        x = Symbol('x')
        # first trend, with more life time, approximately 430-500 hours
        solution = solve(-1.638e-09*x**3 + 2.459e-06*x**2 - 0.001307*x + 2.80324849 - deviation)
        return solution[0]

    def solver_trend2(self, deviation):
        x = Symbol('x')
        # second trend, with less life time, approximately 1000-1200 hours
        solution = solve(-1.713e-08*x**3 + 1.057e-05*x**2 - 0.002527*x + 2.72 - deviation)
        return solution[0]
    
    # lastest instance with highest battery voltage
    def last_high_basis(self, src):  
        # queryset with highest battery and lastest time, we only need information from last packet
        l = SensorBasis.objects.filter(src=src, batt__gt = self.HIGHEST).aggregate(Max('received_date'))

        # instance which filtered by src and most recent received time 
    	try:
            last = SensorBasis.objects.get(src=src, received_date = l['received_date__max'])
            return last
        except Exception:
            print 'No highest value existed'
            return None

    # lastest instance in database
    def lastest_basis(self, src):  
        try:
            last = SensorBasis.objects.filter(src=src).latest('received_date')
            return last
        except Exception:
            print 'No value existed'
            return None

    # battery remaining life
    def remain_life(self, type, batt):
        if type == 0 or type == 1:
            solution = self.solver_trend1(batt)
            life = self.solver_trend1(self.LOWEST)
        elif type == 2:
            solution = self.solver_trend2(batt)
            life = self.solver_trend2(self.LOWEST)

        return life - solution