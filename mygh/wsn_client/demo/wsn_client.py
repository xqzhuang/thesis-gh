#!/usr/bin/python
import logging
import argparse
import sys
import datetime
from email.mime.text import MIMEText
from subprocess import Popen, PIPE

from wsn.clients import WSNClient
from wsn.types import WSNTypes
from wsn.handlers import BaseHandler


class SimpleHandler(BaseHandler):

    def initialize(self):
        pass

    def handle_message(self, message):
	print "sensor #%d battery level is %.2f" % (message['src'], message['batt'])

class DatabaseHandler(BaseHandler):

    def initialize(self):
        """
        Initialize database.
        """
        # glboal database object (fake database)
        self.conn = sqlite3.connect('mygh.db')

        # create history table
        c = self.conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS history
             (hum real, batt real, temp real, src integer, dst integer, received_date integer)''')
        self.conn.commit()

    def handle_message(self, message):
        print "sensor #%d battery level is %.2f" % (message['src'], message['batt'])

        # insert data
        c = self.conn.cursor()
        c.execute("INSERT INTO history VALUES (%f, %f, %f, %d, %d, %d)" 
            % (0, message['batt'], 0, message['src'], message['dst'], 0))
        self.conn.commit()

def start_client(env):

    wsnclient = WSNClient()
    wsnclient.connect(host=env.wsn_host, port=env.wsn_port)

    """
    Battery (low power alarm)

    Handle messages with battery level in it.
    More in BatteryHandler.
    """
    wsnclient.register_pattern(
        data_types = [WSNTypes.SENSOR_BATTERY, ],
        handler_class = SimpleHandler,
        agent_name = 'Message with battery data in it'
    )

    wsnclient.register_pattern(
        handler_class = DatabaseHandler,
        agent_name = 'Message with all data in it'
    )   

    wsnclient.forever()

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format='%(name)s: %(message)s')

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    # parse start parm
    start_parser = subparsers.add_parser('start')
    start_parser.add_argument('--wsn_host', default='plonk.ict.pwr.wroc.pl',help='WSN Server host.')
    start_parser.add_argument('--wsn_port', type=int, default=5001, help='WSN Server port.')
    start_parser.set_defaults(function=start_client)

    args = parser.parse_args(sys.argv[1:])
    args.function(args)
