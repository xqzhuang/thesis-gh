# -*- coding: utf-8 -*-

from Queue import *
import threading

class BaseHandler(object):

    def __init__(self):
        self.queue = Queue()
        self.thread = threading.Thread(target=self.loop)
        self.thread.setDaemon(True)
        self.thread.start()

    def push(self, message):
    	"""
    	Push message to handler queue.
    	This part is extremely important.
    	It allows to create synchronous handling.
    	You can aggregate data in class structure.
    	"""
        self.queue.put(message)

    def loop(self):
    	"""
    	Main loop, handle incomming messages.
    	Synchronized by message queue.
    	"""
        while True:
            message = self.queue.get()
            # call registred handler
            self.handle_message(message)
            # my work is done!
            self.queue.task_done()

    def initialize(self):
    	"""
    	Called after constuction, easy access
    	without overwritte __init__
    	"""
    	pass

    def handle_message(self, message):
    	"""
    	This method has to be overwritten.
    	Will be called when handler will receive message.

    	message - message received from WSN network
    	"""
        pass

