# -*- coding: utf-8 -*- 

class WSNTypes(object):
	"""
	seonsors enum
	"""
	SENSOR_BATTERY  = 1
	SENSOR_TEMP     = 2
	SENSOR_PHOTO_V  = 3
	SENSOR_PHOTO_IR = 4
	SENSOR_HUMIDITY = 5
	SENSOR_PRESSURE = 6
	SENSOR_ACCX     = 7
	SENSOR_ACCY     = 8
	SENSOR_CO       = 9
	SENSOR_CO2      = 10
	SENSOR_DUST     = 11
	SENSOR_PIR      = 12
	SENSOR_MAGNETIC = 13
	SENSOR_VREF	    = 14
	SENSOR_OTHER    = 15

class Hook(object):
	"""
	Structure with handler data.
	All it does is providing easy access to
	given objects.
	"""

	def __init__(self, agent_name, devices_from_ids, devices_to_ids, data_types, handler_instance):
		self.agent_name			= agent_name
		self.devices_from_ids 	= devices_from_ids
		self.devices_to_ids 	= devices_to_ids
		self.data_types 		= data_types
		self.handler_instance 	= handler_instance

	def is_handler(self, device_id, data_types):
		"""
		Check if handler fit message parm
		"""
		return self.is_in_devices_to_ids(device_id) and\
			self.is_in_devices_from_ids(device_id) and\
			self.is_in_data_types(data_types)

	def is_in_devices_from_ids(self, device_id):
		"""
		Check if device src match pattern
		"""
		if not self.devices_from_ids:
			return True
		return device_id in self.devices_from_ids

	def is_in_devices_to_ids(self, device_id):
		"""
		Check if device dst match pattern
		"""
		if not self.devices_to_ids:
			return True
		return device_id in self.devices_to_ids

	def push(self, message):
		"""
		Push message to handler instance queue
		"""
		self.handler_instance.push(message)

	def is_in_data_types(self, data_types):
		"""
		If data type is empty - match all
		"""
		for data_type in self.data_types: 
			if data_type not in data_types:
				return False

		return True