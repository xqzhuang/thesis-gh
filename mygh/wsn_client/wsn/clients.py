# -*- coding: utf-8 -*- 

import threading
import socket
import logging
from .Parser import Parser
from .types import Hook

class WSNClient(object):
	"""
	Client interface.
	"""

	def __init__(self):
		self.socket = None
		self.socket_file = None
		self.hooks = []

		# logger
		self.logger = logging.getLogger('WSNClient')

		super(WSNClient, self).__init__()

	def register_pattern(self, devices_from_ids=[], devices_to_ids=[], data_types=[],\
		data_type=None, handler_class=None,\
		handler_kwargs={}, handler_args=(), agent_name='wsn agent'):
		"""
		Spawns agent for each pattern.

		devices_from_ids	- list of devices ids (message deliver, empty - all)
		devices_to_ids		- list of devices ids (message receivers, empty - all)
		data_types			- list of data types (more in types.py)
		handler_class		- class which will be crate to handle received messages
		handler_kwargs		- dict of arguments passed to handler_class constructor
		handler_args		- tuple of arguments passed to handler_class constructor
		agent_name			- name of handler (for logger)

		ex:

		wsnclient.register_pattern(
	        devices_from_ids = [1,],
	        data_types = [WSNTypes.SENSOR_TEMP, WSNTypes.SENSOR_BATTERY],
	        handler_class = RedirectToTopic,
	        handler_kwargs = {'topic': 'temp',},
	        agent_name = 'PatternName'
    	)
		"""
		# create instance of handler class
		handler_instance = handler_class()
		handler_instance.initialize(
			*handler_args, **handler_kwargs)

		# crate hook
		hook = Hook(agent_name, devices_from_ids, devices_to_ids, data_types, handler_instance)

		self.hooks.append(hook)

	def connect(self, host='dream.ict.pwr.wroc.pl', port=5001):
		"""
		Create daemon thread with connection loop.

		Default server parms:
		host:	dream.ict.pwr.wroc.pl
		port:	5001
		"""
		self.socket = socket.socket()
		self.socket.connect((host, port))
		self.socket_file = self.socket.makefile()

		self.parser = Parser()
		self.parser.unique = True

		self.logger.debug('connected')

		self.server_thread = threading.Thread(target=self.loop)
		self.server_thread.daemon = True
		self.server_thread.start()


	def loop(self):
		"""
		Main loop, handle incoming messages.
		"""
		self.logger.debug('started')
		self.__stopped = False
		while not self.__stopped:
			# read line from socket
			line = self.socket_file.readline()

			# parse line
			d = self.parser.parse_line(line)

			if d == {}:
				continue

			# preven message loop (only unique mode)
			uniq = self.parser.new_pkg(d)
			if (self.parser.unique and uniq) or not self.parser.unique:
				#self.parser.print_packet(d, line, fileout=False, hex_out=False, csv=False)

				# handle only data messages
				if d['msg_type'] is not 2:
					continue

				self.logger.debug('received message [from:%d, to:%d, sensors:%s]',
					d['src'], d['dst'], d['sensors_ids'])

				# loop registred patterns to handle message
				for hook in self.hooks:
					if hook.is_handler(d['src'], d['sensors_ids']):
						self.logger.debug('message handled by %s' % hook.agent_name)
						hook.push(d)

		self.logger.debug('stopped')

	def stop(self):
		"""
		Stop socket daemon.
		"""
		self.__stopped = True

	def forever(self):
		"""
		Run forever, forever young ;(

		This instruction prevents main thread from finishing
		before socked threads will ends or external signal
		will be sent to main thread (KeyboardInterrupt or SystemExit).
		"""
		self.logger.debug("forever")
		try:
			while self.server_thread.isAlive():
				self.server_thread.join(7)
		except KeyboardInterrupt, SystemExit:
			self.logger.debug('Shutting down ...')
			self.close()

	def close(self):
		"""
		Close socket, stops server.
		"""
		if not self.__stopped:
			self.stop()
		
		self.socket.close()
