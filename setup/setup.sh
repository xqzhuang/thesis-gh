#!/bin/bash
cd $PWD
mkdir ghpro
cd ghpro

echo "===Installing git, if it doesn't exist.==="
if [ $(dpkg-query -W -f='${Status}' git 2>/dev/null | grep -c "ok installed") -eq 0 ];
  echo "===git is installed==="
then
  sudo apt-get install git;
fi

echo "====Start to download source code.===="
git clone https://xqzhuang@bitbucket.org/xqzhuang/thesis-gh.git

echo "Download source code has been finished."

echo "===Checking python version. ==="
python --version

echo "===Installing pip, if it doesn't exist.==="
if [ $(dpkg-query -W -f='${Status}' python-pip 2>/dev/null | grep -c "ok installed") -eq 0 ];
  echo "===pip is installed==="
then
  sudo apt-get install python-pip;
fi

echo "===Installing virtual environment, if it doesn't exist.==="
if [ $(dpkg-query -W -f='${Status}' python-virtualenv 2>/dev/null | grep -c "ok installed") -eq 0 ];
  echo "===python-virtualenv is installed==="
then
  sudo apt-get install python-virtualenv;
fi

#start virtual environment
echo "===Start to work in virtual environment.==="
virtualenv env
source env/bin/activate

pip install django
echo "===Comfirm if django is installed.==="
which django-admin.py

#Install django dependencies
cd thesis-gh
pip install -r requirements.txt

#Installing Geospatial libraries
sudo apt-get update

if [ $(dpkg-query -W -f='${Status}' binutils 2>/dev/null | grep -c "ok installed") -eq 0 ];
  echo "===binutils is installed==="
then
  sudo apt-get install binutils;
fi

if [ $(dpkg-query -W -f='${Status}' libproj-dev 2>/dev/null | grep -c "ok installed") -eq 0 ];
  echo "===libproj-dev is installed==="
then
  sudo apt-get install libproj-dev;
fi

if [ $(dpkg-query -W -f='${Status}' gdal-bin 2>/dev/null | grep -c "ok installed") -eq 0 ];
  echo "===libproj-dev is installed==="
then
  sudo apt-get install gdal-bin;
fi

# create static folder
mkdir static

# collecting static files
cd mygh/
./manage.py collectstatic
